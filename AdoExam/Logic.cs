﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace AdoExam
{
    public class Logic
    {
        public ExamContext Context { get; set; }
        List<User> UserList { get; set; }

        public Logic(ExamContext context)
        {
            Context = context;
            UserList = context.Users.ToList();
        }

        public bool Recovery()
        {
            Console.WriteLine("Введите имя пользователя: ");
            string userName = Console.ReadLine();
            string phoneNumber = "0";

            foreach(var item in UserList)
            {
                if (item.UserName == userName)
                    phoneNumber = item.PhoneNumber;
            }

            if(phoneNumber != "0")
            {
                string sendedCode = SendMessage(phoneNumber);

                Console.WriteLine("Введите отправленный на ваш номер код: ");

                string enteredCode = Console.ReadLine();

                if (sendedCode == enteredCode)
                {
                    Console.WriteLine("Пользователь восстановлен!");
                    return true;
                }
                else return false;
            }
            else return false;
        }

        public bool Registration(User user)
        {

            bool ableToRegistration = true;

            Console.WriteLine("Имя пользователя:");
            string userName = Console.ReadLine();
            Console.WriteLine("Номер телефона:");
            string phoneNumber = Console.ReadLine();

            foreach (var item in UserList)
            {
                if (item.PhoneNumber == phoneNumber)
                    ableToRegistration = false;
            }

            if(ableToRegistration == true)
            {
                user.UserName = userName;
                user.PhoneNumber = phoneNumber;

                string sendedCode = SendMessage(phoneNumber);

                Console.WriteLine("Введите отправленный на ваш номер код: ");

                string enteredCode = Console.ReadLine();

                if(enteredCode == sendedCode)
                {
                    Context.Users.Add(user);

                    Context.SaveChanges();

                    return true;
                }
                else
                {
                    return false;
                }

                
            }
            else
            {
                return false;
            }
        }

        public string SendMessage(string phoneNumber)
        {
            Random rand = new Random();
            int codeInt = rand.Next(1000, 9999);

            string code = codeInt.ToString();

            // Find your Account Sid and Token at twilio.com/console
            const string accountSid = "AC849627a77162e82a62388508929a3497";
            const string authToken = "251555ec3e87e0f53709251d57acfd60";

            TwilioClient.Init(accountSid, authToken);

            var message = MessageResource.Create(
                from: new Twilio.Types.PhoneNumber("(859) 657-8234 "),
                body: code,
                to: new Twilio.Types.PhoneNumber(phoneNumber)
            );

            Console.WriteLine(message.Sid);

            return code;
        }
    }
}
