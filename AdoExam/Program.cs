﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoExam
{
    class Program
    {
        static void Main(string[] args)
        {

            //Как мне показалось, одно из условий задания лишнее или задание с подвохом.
            //Я не посчитал нужным тут использовать Lazy Loading, потому что он используется для того, чтобы ПРИ НЕОБХОДИМОСТИ подгружать объекты из навигационного свойства используемого объекта.
            //Но у меня один лишь класс User, и соответсвенно, нафигационных свойств он иметь не может.
            //
            //Код потверждения отправляется на мой номер (+77029069632), если нужно, можете написать мне, я скину вам код подтверждения)

            ExamContext context = new ExamContext();
            Logic logic = new Logic(context);
            User user = new User();

            while (true)
            {
                Console.Clear();
                Console.WriteLine("1. Регистрация");
                Console.WriteLine("2. Восстановление");

                int menu;
                Int32.TryParse(Console.ReadLine(), out menu);

                switch (menu)
                {
                    case 1:
                        Console.Clear();
                        if (logic.Registration(user)) Console.WriteLine("Регистрация прошла успешно!");
                        else Console.WriteLine("Не удалось зарегистрироваться!");
                        Console.ReadLine();
                        break;
                    case 2:
                        Console.Clear();
                        if (logic.Recovery()) Console.WriteLine("Пользователь восстановлен!");
                        else Console.WriteLine("К данному пользователю не привязан номер!");
                        Console.ReadLine();
                        break;
                }
            }
        }
    }
}
