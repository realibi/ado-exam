﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoExam
{
    public class User
    {
        public int Id { get; set; }
        public virtual string UserName { get; set; }
        public virtual string PhoneNumber { get; set; }
    }
}
